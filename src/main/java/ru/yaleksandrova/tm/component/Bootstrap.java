package ru.yaleksandrova.tm.component;

import ru.yaleksandrova.tm.api.controller.ICommandController;
import ru.yaleksandrova.tm.api.controller.IProjectController;
import ru.yaleksandrova.tm.api.controller.ITaskController;
import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.api.sevice.ICommandService;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.api.sevice.IProjectTaskService;
import ru.yaleksandrova.tm.api.sevice.ITaskService;
import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.constant.ArgumentConst;
import ru.yaleksandrova.tm.controller.CommandController;
import ru.yaleksandrova.tm.controller.ProjectController;
import ru.yaleksandrova.tm.controller.TaskController;
import ru.yaleksandrova.tm.repository.CommandRepository;
import ru.yaleksandrova.tm.repository.ProjectRepository;
import ru.yaleksandrova.tm.repository.TaskRepository;
import ru.yaleksandrova.tm.service.CommandService;
import ru.yaleksandrova.tm.service.ProjectService;
import ru.yaleksandrova.tm.service.ProjectTaskService;
import ru.yaleksandrova.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {

    private final CommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService, projectService);

    public void start(String[] args) {
        System.out.println("** Welcome to Task Manager **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        switch(arg) {
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            default: commandController.showErrorArgument();
        }
    }

    public void parseCommand(final String command) {
        switch(command) {
            case ApplicationConst.ABOUT: commandController.showAbout(); break;
            case ApplicationConst.VERSION: commandController.showVersion(); break;
            case ApplicationConst.HELP: commandController.showHelp(); break;
            case ApplicationConst.INFO: commandController.showInfo(); break;
            case ApplicationConst.EXIT: commandController.exitApplication(); break;
            case ApplicationConst.COMMANDS: commandController.showCommands(); break;
            case ApplicationConst.ARGUMENTS: commandController.showArguments(); break;
            case ApplicationConst.TASK_LIST: taskController.showTasks(); break;
            case ApplicationConst.TASK_CREATE: taskController.createTask(); break;
            case ApplicationConst.TASK_CLEAR: taskController.clearTask(); break;
            case ApplicationConst.PROJECT_LIST: projectController.showProjects(); break;
            case ApplicationConst.PROJECT_CREATE: projectController.createProject(); break;
            case ApplicationConst.PROJECT_CLEAR: projectController.clearProject(); break;
            case ApplicationConst.PROJECT_SHOW_BY_ID: projectController.showById(); break;
            case ApplicationConst.PROJECT_SHOW_BY_INDEX: projectController.showByIndex(); break;
            case ApplicationConst.PROJECT_UPDATE_BY_ID: projectController.updateById(); break;
            case ApplicationConst.PROJECT_UPDATE_BY_INDEX: projectController.updateByIndex(); break;
            case ApplicationConst.PROJECT_REMOVE_BY_ID: projectController.removeById(); break;
            case ApplicationConst.PROJECT_REMOVE_BY_INDEX: projectController.removeByIndex(); break;
            case ApplicationConst.PROJECT_REMOVE_BY_NAME: projectController.removeByName(); break;
            case ApplicationConst.TASK_SHOW_BY_ID: taskController.showById(); break;
            case ApplicationConst.TASK_SHOW_BY_INDEX: taskController.showByIndex(); break;
            case ApplicationConst.TASK_UPDATE_BY_ID: taskController.updateById(); break;
            case ApplicationConst.TASK_UPDATE_BY_INDEX: taskController.updateByIndex(); break;
            case ApplicationConst.TASK_REMOVE_BY_ID: taskController.removeById(); break;
            case ApplicationConst.TASK_REMOVE_BY_INDEX: taskController.removeByIndex(); break;
            case ApplicationConst.TASK_REMOVE_BY_NAME: taskController.removeByName(); break;
            case ApplicationConst.TASK_START_BY_ID: taskController.startById(); break;
            case ApplicationConst.TASK_START_BY_INDEX: taskController.startByIndex(); break;
            case ApplicationConst.TASK_START_BY_NAME: taskController.startByName(); break;
            case ApplicationConst.TASK_FINISH_BY_ID: taskController.finishById(); break;
            case ApplicationConst.TASK_FINISH_BY_INDEX: taskController.finishByIndex(); break;
            case ApplicationConst.TASK_FINISH_BY_NAME: taskController.finishByName(); break;
            case ApplicationConst.TASK_CHANGE_STATUS_BY_ID: taskController.changeStatusById(); break;
            case ApplicationConst.TASK_CHANGE_STATUS_BY_INDEX: taskController.changeStatusByIndex(); break;
            case ApplicationConst.TASK_CHANGE_STATUS_BY_NAME: taskController.changeStatusByName(); break;
            case ApplicationConst.TASK_LIST_SHOW_BY_PROJECT_ID: taskController.findAllTaskByProjectId(); break;
            case ApplicationConst.TASK_BIND_TO_PROJECT: taskController.bindTaskToProjectById(); break;
            case ApplicationConst.TASK_UNBIND_FROM_PROJECT: taskController.unbindTaskById(); break;
            case ApplicationConst.PROJECT_START_BY_ID: projectController.startById(); break;
            case ApplicationConst.PROJECT_START_BY_INDEX: projectController.startByIndex(); break;
            case ApplicationConst.PROJECT_START_BY_NAME: projectController.startByName(); break;
            case ApplicationConst.PROJECT_FINISH_BY_ID: projectController.finishById(); break;
            case ApplicationConst.PROJECT_FINISH_BY_INDEX: projectController.finishByIndex(); break;
            case ApplicationConst.PROJECT_FINISH_BY_NAME: projectController.finishByName(); break;
            case ApplicationConst.PROJECT_CHANGE_STATUS_BY_ID: projectController.changeStatusById(); break;
            case ApplicationConst.PROJECT_CHANGE_STATUS_BY_INDEX: projectController.changeStatusByIndex(); break;
            case ApplicationConst.PROJECT_CHANGE_STATUS_BY_NAME: projectController.changeStatusByName(); break;
            default: commandController.showErrorCommand();
        }
    }

    public void  parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
    }

}
