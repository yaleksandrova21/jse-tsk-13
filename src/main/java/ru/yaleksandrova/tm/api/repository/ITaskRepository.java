package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    boolean existsById(String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task startById(String id);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task bindTaskToProjectById(String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String projectId);

    Task unbindTaskById(String taskId);

    void removeAllTaskByProjectId(String projectId);

}
