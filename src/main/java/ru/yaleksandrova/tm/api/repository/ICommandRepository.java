package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
