package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public Task findById(final String id) {
        for(Task task:tasks){
            if(id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(String name) {
        for(Task task:tasks){
            if(name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task removeById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        final List<Task> listByProject = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        if (listByProject.size() > 0) return listByProject;
        else return null;
    }

    @Override
    public Task unbindTaskById(String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        List<Task> newTasksList = new ArrayList<>();
        for (final Task task: tasks) {
            if (!projectId.equals(task.getProjectId())) newTasksList.add(task);
        }
        tasks = newTasksList;
    }

}
