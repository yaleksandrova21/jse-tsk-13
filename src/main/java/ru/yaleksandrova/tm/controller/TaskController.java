package ru.yaleksandrova.tm.controller;

import ru.yaleksandrova.tm.api.controller.ITaskController;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.api.sevice.ITaskService;
import ru.yaleksandrova.tm.api.sevice.IProjectTaskService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    private final IProjectService projectService;

    public TaskController(final ITaskService taskService, IProjectTaskService projectTaskService, IProjectService projectService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.projectService = projectService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task: tasks) System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTask(Task task) {
        if (task == null)
            return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        this.taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER ID]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.findByIndex(index);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void removeById() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.removeByIndex(index);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Task task  = taskService.removeByName(name);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }

    @Override
    public void updateById() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Task task  = taskService.findById(id);
        if (task == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedId = taskService.updateById(id, name, description);
        if (taskUpdatedId == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedIndex = taskService.updateByIndex(index, name, description);
        if (taskUpdatedIndex == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

    @Override
    public void startById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Task task = taskService.startById(id);
        if(task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void startByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.startByIndex(index);
        if(task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void startByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Task task = taskService.startByName(name);
        if(task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void finishById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Task task = taskService.finishById(id);
        if(task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void finishByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.finishByIndex(index);
        if(task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void finishByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if(task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void changeStatusByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByName(name, status);
        if (task == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void findAllTaskByProjectId() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = ApplicationUtil.nextLine();
        final List<Task> tasks = projectTaskService.findTaskByProjectId(projectId);
        if (tasks == null) System.out.println("Incorrect value!");
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("[ENTER TASK ID]");
        final String taskId = ApplicationUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) {
            System.out.println("Incorrect value!");
        }
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = ApplicationUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
        final Task taskUpdated = projectTaskService.bindTaskById(projectId, taskId);
    }

    @Override
    public void unbindTaskById() {
        System.out.println("[ENTER TASK ID]");
        final String taskId = ApplicationUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) {
            System.out.println("Incorrect value!");
        }
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = ApplicationUtil.nextLine();
        final Task taskUnbind = projectTaskService.unbindTaskById(projectId, taskId);
    }

}
